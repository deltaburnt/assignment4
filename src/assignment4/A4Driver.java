/* EE422C Assignment 4 */
/* Student Name: Cassidy Burden (eid: cab5534), Lab Section: Xavier (Th 9:30-11:00)*/

package assignment4;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class A4Driver
{

	public static void main(String[] args)
	{
		/*try
		{
			Random rand = new Random();
			BufferedWriter write = new BufferedWriter(new FileWriter("input.txt"));
			for(int i = 0; i < 10000; i++)
			write.write(rand.nextInt() + "\n");
			write.flush();
			write.close();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		Experimenter experiment = new Experimenter();
		experiment.experiment1();
		experiment.experiment2();
		experiment.experiment3();
		experiment.experiment4();
		experiment.experiment5();
		experiment.experiment6();
	}

}
