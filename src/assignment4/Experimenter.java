package assignment4;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;
import java.util.Collections;

public class Experimenter
{
	private BufferedReader input;
	private StopWatch watch = new StopWatch();
	private int invalidInputs = 0;
	private int foundInputs = 0;
	private int notFoundInputs = 0;
	
	ArrayList<Integer> list = new ArrayList<Integer>();
	Random rand = new Random();
	
	public Experimenter()
	{		
		rand.setSeed(15485863);
		
		for(int i = 0; i < (int) 1000000; i++)
		{
			list.add(rand.nextInt(Integer.MAX_VALUE));
		}
	}

	public void experiment1()
	{
		reset();
		ArrayList<Integer> input = getInput();
		
		watch.start();
		for(Integer inputInt : input)
		{
			boolean found = false;
			if(inputInt < 0) { invalidInputs++; continue; }
			
			if (list.indexOf(inputInt) >= 0) { foundInputs++; }
			
			else { notFoundInputs++; }
		}
		watch.stop();
		
		System.out.println("Experiment 1 Results:");
		experimentOutput();
	}
	
	public void experiment2()
	{
		reset();
		ArrayList<Integer> input = getInput();
		
		LinkedList<Integer> list2 = new LinkedList<Integer>(list);
		
		watch.start();
		for(Integer inputInt : input)
		{
			boolean found = false;
			if(inputInt < 0) { invalidInputs++; continue; }
			
			if (list2.indexOf(inputInt) >= 0) { foundInputs++; }
			
			else { notFoundInputs++; }
		}
		watch.stop();
		
		System.out.println("Experiment 2 Results:");
		experimentOutput();
	}
	
	public void experiment3()
	{
		reset();
		ArrayList<Integer> input = getInput();
		
		watch.start();
		Collections.sort(list);
		watch.stop();
		
		System.out.println("Experiment 3 Results:");
		System.out.println("Time elapsed in nanoseconds is: " + watch.getElapsedTime());
		System.out.println("Time elapsed in seconds is: " + watch.getElapsedTime() / StopWatch.NANOS_PER_SEC);
		System.out.println();
	}
	
	public void experiment4()
	{
		reset();
		ArrayList<Integer> input = getInput();
		
		watch.start();
		for(Integer inputInt : input)
		{
			if(inputInt < 0) { invalidInputs++; continue; }

			int index = Collections.binarySearch(list, inputInt);
			if (index >= 0) { foundInputs++; }
			else { notFoundInputs++; }
		}
		watch.stop();
		
		System.out.println("Experiment 4 Results:");
		experimentOutput();
	}
	
	public void experiment5()
	{
		reset();
		ArrayList<Integer> input = getInput();
		
		watch.start();
		for(Integer inputInt : input)
		{
			if(inputInt < 0) { invalidInputs++; continue; }

			int index = interpolationSearch(inputInt);
			if (index >= 0) { foundInputs++; }
			else { notFoundInputs++; }
		}
		watch.stop();
		
		System.out.println("Experiment 5 Results:");
		experimentOutput();
	}
	
	public void experiment6()
	{
		reset();
		ArrayList<Integer> input = getInput();
		
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>(2000000, 0.50f);
		for(int i = 0; i < list.size(); i++) { map.put(list.get(i), 0); }
		
		watch.start();
		for(Integer inputInt : input)
		{
			if(inputInt < 0) { invalidInputs++; continue; }

			if (map.containsKey(inputInt))
			{
				map.put(inputInt, map.get(inputInt) + 1);
				foundInputs++;
			}
			else { notFoundInputs++; }
		}
		watch.stop();
		
		System.out.println("Experiment 6 Results:");
		experimentOutput();
	}
	
	private int interpolationSearch(int search)
	{
		int low = 0;
		int high = list.size() - 1;
		int mid = 0;
		
		while (search >= list.get(low) && search <= list.get(high))
		{
			mid = low + (int) Math.ceil(((long) (search - list.get(low)) * (high - low)) / (list.get(high) - list.get(low)));
			
			if(search == list.get(mid)) { return mid; }
			else if (search > list.get(mid)) { low = mid + 1; }
			else if (search < list.get(mid)) { high = mid - 1; }
		}
		
		if (search == list.get(low)) { return low; }
		return -1;
	}
	
	private void experimentOutput()
	{
		System.out.println("Number of found values: " + foundInputs);
		System.out.println("Number of values not found: " + notFoundInputs);
		System.out.println("Number of illegal values in input file: " + invalidInputs);
		System.out.println("Time elapsed in nanoseconds is: " + watch.getElapsedTime());
		System.out.println("Time elapsed in seconds is: " + watch.getElapsedTime() / StopWatch.NANOS_PER_SEC);
		System.out.println();
	}
	
	
	public void reset()
	{
		watch.reset();
		invalidInputs = foundInputs = notFoundInputs = 0;
		try
		{
			if (input != null) { input.close(); }
			input = new BufferedReader(new FileReader("input.txt"));
		}
		catch (FileNotFoundException e)
		{
			System.out.println("input.txt not found. Exiting...");
			System.exit(-1);
		}
		catch (IOException e)
		{
			System.out.println("IO Exception caught. Exitting...");
			System.exit(-1);
		}
	}
	
	public ArrayList<Integer> getInput()
	{
		String line = "";
		ArrayList<Integer> inputList = new ArrayList<Integer>();
		
		while(true)
		{
			try
			{
				line = input.readLine();
				if(line == null) { break; }
				
				Integer inputInt = Integer.parseInt(line);
				
				inputList.add(inputInt);
			}
			catch (IOException e)
			{
				System.out.println("IO Exception caught. Exitting...");
				System.exit(-1);
			}
			catch (NumberFormatException e)
			{
				inputList.add(-1);
			}
		}
		
		return inputList;
	}
}
